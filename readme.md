# Betterrev Introduction#

A friendly, hosted wrapper around OpenJDK Contributions and 'webrevs'. See the [Wiki](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Home) for details on the [Grand Plan](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Test%20and%20CI),[Workflow](https://bitbucket.org/adoptopenjdk/betterrev/wiki/Work%20Flow) and more. Join the mailing list to participate in the discussions!

* http://mail.openjdk.java.net/mailman/listinfo/adoption-discuss

# Assumptions #

We are assuming that you know how to use Git, Mecurial, BitBucket etc. If not, don't panic!  Just drop an email to adoption-discuss@openjdk.java.net.

# Getting started for developers #

The project has two branches, **master** and **javaee**. The **master** branch is an abandoned Play Framework based implementation. The **javaee** branch is the new active Java EE 7 and HTML5 based branch.  If you want specific instructions for Linux you can try the specific [[Linux_Install_Instructions]].


## Install Source Code ##

* Install [Git](http://git-scm.com/downloads) for your operating system. 
* Install [Mercurial](http://mercurial.selenic.com/) for you operating system.
* [Fork the betterrev repository](https://bitbucket.org/adoptopenjdk/betterrev/fork) - **WARNING** Untick the **issues** and **wiki** checkboxes, you do not want those!
* Clone your fork onto your local file system. For more info on this method of interacting with Bitbucket via SSH please read [this bitbucket wiki article](https://confluence.atlassian.com/display/BITBUCKET/Set+up+SSH+for+Git))

`git clone git@bitbucket.org:<your_username>/betterrev.git betterrev_project`

* Clone the adopt repository into the *betterrev_project/betterrev* directory, e.g.:
```
cd betterrev_project/betterrev 
hg clone https://bitbucket.org/adoptopenjdk/adopt adopt
```

* From within the *betterrev_project/betterrev/adopt* directory, clone the jdk9 repository, e.g.: 

```
cd adopt
hg clone https://bitbucket.org/adoptopenjdk/jdk9 jdk9
```
* Get the jdk9 sources:
    * `$ cd jdk9` 
    * `$ chmod u+x get_source.sh`
    * `$ ./get_source.sh`

* Your final directory structure should look something like the following:

```
betterrev_project\
    readme.md
    LICENSE
    betterrev\
        adopt\
            jdk9\
                <various OpenJDK folders>
        src\
            main\
                angularapp\
                java\
            test\
        target\
```

## Install Serverside Tools ##

* Install [Java](http://www.oracle.com/technetwork/java/javase/downloads/index.html) - 1.8.0_31 at time of writing
* Install [Maven](http://maven.apache.org/) - 3.2.5 at time of writing
* Install an IDE ([IntelliJ](https://www.jetbrains.com/idea/) is recommended, but you can Netbeans or Eclipse it if you like)
* Install Wildfly Application Server ([Wildfly](http://wildfly.org/downloads/) - 8.2.0.Final at time of writing
* TODO [[PostgreSQL on-disk database]]

## Install Clientside Tools ##

### Install Chrome/Chromium ###

* Install Chrome(http://www.google.com/chrome/)

### Install node.js and npm  ###

* Install [Node JS](http://nodejs.org/) - Serverside Javascript engine - 1.0.35 at time of writing
* Install [NPM](https://www.npmjs.com/) - Package manager for NodeJS - 2.0.20 at time of writing

### Install bower & gulp globally using npm ###

Install this globally, i.e.

```sudo npm install -g bower```
```sudo npm install gulp -g```

### Install bower & gulp globally using npm ###

In *betterrev/src/main/angularapp*

```bower install```

When prompted **select option 2** which will install **angular 1.3.6**

# Running the App #

## Full Application - Run Maven ##

```
cd betterrev
mvn clean install
```

You can then deploy the application to the Wildfly server and goto:

http://localhost:8080/betterrev-1.0.0-SNAPSHOT/#/index

## Frontend only - Run gulp  ##

This is useful for rapid development of the UI portion of betterrev

```
cd betterrev/src/main/angularapp
gulp
```

Then go to http://localhost:8090/#/index

TBA