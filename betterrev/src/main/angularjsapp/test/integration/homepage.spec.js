var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Home Page Tests', function() {

    // Look for the id of "homeHeading"
    it('Home Page exists', function() {
        browser.get('http://127.0.0.1:8090/#/index');
        expect(element(protractor.By.id("homeHeading")).isDisplayed()).to.eventually.equal(true);
    });

});
