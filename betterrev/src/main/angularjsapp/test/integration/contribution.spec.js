var chai = require('chai');
var expect = chai.expect;

chai.use(require('chai-as-promised'));

describe('Contribution Page Tests', function() {

    // Look for the id of "contributionsTable"
    it('Contribution Div exists', function() {
        browser.get('http://127.0.0.1:8090/#/contributions/1');
        expect(element(protractor.By.id("contributionDiv")).isDisplayed()).to.eventually.equal(true);

    });

});