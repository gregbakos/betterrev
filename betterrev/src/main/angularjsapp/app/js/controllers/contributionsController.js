angular.module('betterrev').controller('ContributionsController', function($scope) {

    // TODO This is test data - obviously we want this to come from the backend
    $scope.contributions = [ {
        "id" : "1",
        "name" : "Test Contribution",
        "repositoryId" : "jdk9",
        "requester" : {
            "bitbucketUserName" : "Martijn Verburg BB",
            "name" : "Martijn Verburg",
            "ocaStatus" : {
                "displayName" : "Martijn Verburg OCA"
            }
        },
        "mentors" : [ {
            "name" : "Stuart Marks",
            "email" : "stuart.marks@oracle.com"
        }, {
            "name" : "John Oliver",
            "email" : "john@jclarity.com"
        } ],
        "tags" : {
            "tag" : {
                "name" : "Test Data"
            }
        },
        "contributionEvents" : [ {
            "CONTRIBUTION_GENERATED" : "CONTRIBUTION_GENERATED"
        }, {
            "CONTRIBUTION_MODIFIED" : "CONTRIBUTION_MODIFIED"
        } ],

        // TODO Better test data for the 2 below
        "pullRequestUrlForOwner" : "https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/%s/diff",
        "webrevLocation" : "public/webrevs/webrev-%s-%s/",
        "createdOn" : 1410620614000,
        "updatedOn" : 1410620614000,
        "pullRequestId" : "10",
        "state" : "OPEN",
        // Default branch was a method call - but we can pre-calculate and send
        // as JSON
        "description" : "This is test data coming from a static Javascript file",
        "isDefaultBranch" : "false"
    } ];

});