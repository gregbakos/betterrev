var gulp = require('gulp');

/*
 var flatten = require('gulp-flatten');
 var ngHtml2js = require('gulp-ng-html2js');
 var sass = require('gulp-sass');
 var gulpif = require('gulp-if');
 */

var protractor = require('gulp-protractor').protractor;
var debug = require('gulp-debug');
var connect = require('gulp-connect');
var karma = require('gulp-karma');
var streamqueue = require('streamqueue');
var bowerFiles = require('gulp-bower-files');
// TODO Gulp says to use the below instead of gulp-bower-files
var mainBowerFiles = require('main-bower-files');
var watch = require('gulp-watch');
var gzip = require('gulp-gzip');
var concat = require('gulp-concat');
var uglify = require('gulp-uglifyjs');
var ngmin = require('gulp-ngmin');
var clean = require('gulp-clean');
var runSequence = require('run-sequence');
var rename = require('gulp-rename');
var cssmin = require('gulp-cssmin');
var lazypipe = require('lazypipe');

var fs = require('fs');

var environment = 'development';
var noop = function () {
};

var target_dir = '../webapp';
var tmp_dir = target_dir + '/tmp';

function createDir(the_dir) {
    if (!fs.existsSync(the_dir)) {
        fs.mkdirSync(the_dir, function (err) {
            if (err) {
                console.log('Cannot create folder ' + the_dir, err);
                throw err;
            }
        });
    }
}

createDir(target_dir);
createDir(tmp_dir);

// Clean all the temp and dist files
gulp.task('clean', function () {
    return gulp.src(
            [target_dir],
            {
                read: false
            }
    )
            .pipe(clean({force: true}));
});

gulp.task('connect', function () {

    connect.server({
        root: [target_dir],
        livereload: true,
        port: 8090
    });
});

gulp.task('integration-connect', function () {

    connect.server({
        root: [target_dir],
        livereload: false,
        port: 8090
    });
});


// Copy built files to the dist folder
gulp.task('assets', function () {
    gulp.src('./public/**/*')
            .pipe(gulp.dest(target_dir));
});

//Copy built files to the dist folder
gulp.task('partials', function () {
    gulp.src('./app/partials/**/*')
            .pipe(gulp.dest(target_dir + '/partials'));
});

// lazypipe is used for a delay execution
var cssminTask = lazypipe()
        .pipe(cssmin)
        .pipe(rename, {
            suffix: '.min'
        })
        .pipe(gulp.dest, target_dir);

gulp.task('app', [], function () {
    // it concats all the application files
    var files = gulp.src(['./app/**/*.js'])
            .pipe(ngmin());
    return processJsFiles('js/app', files);
});

//Concats files from bower and few custom scripts into lib.js
gulp.task('lib', function () {
    var files = streamqueue({objectMode: true},
    bowerFiles({
        paths: {
            bowerJson: './bower.json'
        },
        env: 'lib'
    })
            );

    return processJsFiles('js/lib', files);
});

// TODO replicate the lib task using main-bower
gulp.task('lib2', function () {
    mainBowerFiles({
        paths: {
            bowerDirectory: './bower_components',
            bowerJson: './bower.json'
        }
    })
            .pipe(gulp.dest('js/lib'));
});

// Builds all the javascript files
gulp.task('build-js', ['lib', 'app'], function () {
});

// Builds all components
gulp.task('build', ['assets', 'partials', 'build-js']);

//It cleans and builds (called by Maven)
gulp.task('dist', ['clean-build-test']);

// It cleans and builds
gulp.task('clean-build', function (callback) {
    runSequence('clean',
            'build',
            callback);
});

// It cleans and builds and tests
gulp.task('clean-build-test', function (callback) {
    runSequence('clean-build',
            'unit-test',
            'integration-test',
            callback);
});

gulp.task('watch', function () {

    watch('./public/**', function () {
        gulp.start(['assets']);
    }).pipe(connect.reload());

    watch('./app/js/**', function () {
        gulp.start(['app']);
    }).pipe(connect.reload());
    ;

    watch('./app/partials/**', function () {
        gulp.start(['partials']);
    }).pipe(connect.reload());
    ;
});

// Clean, build and watch.
gulp.task('default', function (callback) {
    runSequence('clean-build',
            'connect',
            'watch',
            callback);
});

gulp.task('unit-test', function () {
    // Be sure to return the stream
    return gulp.src(['random text because gulp is weird'])
            .pipe(debug({verbose: true}))
            .pipe(karma({
                configFile: 'test/unit/karma.conf.js'
            }))
            .on('error', function (err) {
                throw err;
            });
});

gulp.task('disconnect', function() {
    connect.serverClose();
    }
);

// Run integration tests.
gulp.task('integration-test', function () {
    runSequence('integration-connect', 'run-integration-test', 'disconnect');
});

gulp.task('run-integration-test', function () {
    return gulp.src(['random text because gulp is weird'])
            .pipe(debug({verbose: true}))
            .pipe(protractor({
                configFile: 'test/integration/protractor.conf.js'
            }))
            .on('error', function (err) {
                throw err;
            });
});

function processJsFiles(name, files) {
    return files.pipe(concat(name + '.js'))
            .pipe(gulp.dest(target_dir))
            .pipe(gzip())
            .pipe(gulp.dest(target_dir));
}
