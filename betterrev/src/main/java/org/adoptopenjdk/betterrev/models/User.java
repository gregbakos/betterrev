package org.adoptopenjdk.betterrev.models;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * User entity class, which represents all Users within the BetterRev
 * application.
 */
@Entity
@Table(name = "betterrev_user")
public class User {

    public enum OcaStatus {
        AWAITING_APPROVAL,
        SIGNED, 
        NOT_SIGNED, 
        UNKNOWN;
        
        public String displayName() {
            return this.name().replaceAll("_", " ").toLowerCase();
        }
    }

    public User() {}
    
    // TODO get by ID the Java EE Way
    //public static Model.Finder<Long, User> find = new Model.Finder<>(Long.class, User.class);

    // TODO do the Java EE Way
    /*
    public static User findOrCreate(String bitbucketUserName, String displayName) {
        User user = find.where().eq("bitbucketUserName", bitbucketUserName).findUnique();
        if (user == null) {
            user = new User(bitbucketUserName, displayName, OcaStatus.UNKNOWN);
            user.save();
        }
        return user;
    }
    */
    public static User findOrCreate(String bitbucketUserName, String displayName) {
        return null;
    }
    
    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @NotNull
    public String name;

    @NotNull
    public String bitbucketUserName;

    public String openjdkUsername;

    @Enumerated(EnumType.STRING)
    public OcaStatus ocaStatus;

    public Date createdDate;

    public User(String bitbucketUserName, String name, OcaStatus ocaStatus) {
        this.bitbucketUserName = bitbucketUserName;
        this.name = name;
        this.ocaStatus = ocaStatus == null ? OcaStatus.UNKNOWN : ocaStatus;
        this.createdDate = new Date();
    }

    @Override
    public int hashCode() {
        return bitbucketUserName.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!super.equals(obj)) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }

        User other = (User) obj;
        return Objects.equals(bitbucketUserName, other.bitbucketUserName);
    }

    public Long getKey() {
        return id;
    }

    public void setKey(Long key) {
        id = key;
    }
}
