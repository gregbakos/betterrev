package org.adoptopenjdk.betterrev.models;

import java.time.LocalDateTime;

import org.adoptopenjdk.betterrev.events.BetterrevEvent;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import static javax.persistence.GenerationType.IDENTITY;

/**
 * TODO - This really needs to be an event or a model but not both
 * 
 * ContributionEvent that represents a lifecycle event associated with a specific
 * Contribution.
 */
@Entity
public class ContributionEvent extends BetterrevEvent {

    // TODO get by ID the Java EE Way
    // public static Model.Finder<Long, ContributionEvent> find = new Model.Finder<>(Long.class, ContributionEvent.class);

    @GeneratedValue(strategy = IDENTITY)
    @Id
    public Long id;

    @ManyToOne
    public Contribution contribution;

    @Enumerated(EnumType.STRING)
    public ContributionEventType contributionEventType;

    public String linkToExternalInfo;

    public LocalDateTime createdDate;

    
    // TODO JPA forces public no args constructor
    public ContributionEvent() {
        this.createdDate = LocalDateTime.now();
    }

    public ContributionEvent(ContributionEventType contributionEventType) {
        this();
        this.contributionEventType = contributionEventType;
    }

    public ContributionEvent(ContributionEventType contributionEventType, Contribution contribution) {
        this(contributionEventType);
        this.contribution = contribution;
    }

    public ContributionEvent(ContributionEventType contributionEventType, String linkToExternalInfo) {
        this(contributionEventType);
        this.linkToExternalInfo = linkToExternalInfo;
    }

    @Override
    public String toString() {
        return "ContributionEvent [id=" + id + ", contribution=" + contribution + ", contributionEventType="
                + contributionEventType + ", linkToExternalInfo=" + linkToExternalInfo + ", createdDate=" + createdDate
                + "]";
    }

    public Long getKey() {
        return id;
    }

    public void setKey(Long key) {
        id = key;
    }
}
