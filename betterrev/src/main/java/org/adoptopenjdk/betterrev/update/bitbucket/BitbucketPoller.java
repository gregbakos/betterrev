package org.adoptopenjdk.betterrev.update.bitbucket;

import java.util.Properties;

import javax.ws.rs.core.Response;

import org.adoptopenjdk.betterrev.update.BetterrevActor;
import org.adoptopenjdk.betterrev.update.pullrequest.ImportPullRequestsEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.JsonNode;

/**
 * Polls the Bitbucket API and delegates to the PullRequestImporter class
 */
public class BitbucketPoller extends BetterrevActor {

    private final static Logger LOGGER = LoggerFactory.getLogger(BitbucketPoller.class); 
    
    private static final int ONE_SECOND_TIMEOUT = 1000;
    private static final String API_URL = "https://bitbucket.org/api/2.0/repositories/%s/%s/pullrequests/";

    @Override
    public void onReceive(Object message) {
        if (message instanceof PollBitbucketEvent) {
            LOGGER.debug("PollBitbucketEvent received.");

            // TODO replace with somesort of global configuration
            //Configuration configuration = Play.application().configuration();
            //String owner = configuration.getString("owner");            
            Properties configuration = new Properties();
            String owner = configuration.getProperty("owner");
            
            //for (String projectToken : configuration.getString("projects").split(" ")) {
            for (String projectToken : configuration.getProperty("projects").split(" ")) {
                String project = projectToken.trim();
                LOGGER.info(String.format("Polling bitbucket with owner '%s' and project '%s'", owner, project));

                // TODO replace with Java EE response
                /* WSResponse response = WS.url(String.format(API_URL, owner, project)).get().get(ONE_SECOND_TIMEOUT);
                JsonNode responseJson = response.asJson();
                if ((response.getStatus() != Http.Status.OK) || responseJson == null) {
                    LOGGER.error("Bitbucket did not return a valid response on the current execution of run()...");
                } else {
                    eventStream().publish(new ImportPullRequestsEvent(responseJson, project));
                }
                */
            }
        }
    }
}
