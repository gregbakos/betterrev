package org.adoptopenjdk.betterrev.update.ocachecker;

import org.adoptopenjdk.betterrev.models.ContributionEvent;
import org.adoptopenjdk.betterrev.models.User;
import org.adoptopenjdk.betterrev.models.User.OcaStatus;
import org.adoptopenjdk.betterrev.utils.OracleContributorAgreementService;
import org.adoptopenjdk.betterrev.utils.exception.OCAComunicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CheckOcaStatus {

    private final static Logger LOGGER = LoggerFactory.getLogger(CheckOcaStatus.class);
    
    public static boolean check(ContributionEvent request) {
        User user = request.contribution.requester;
        boolean hasSigned = false;
        if (user.ocaStatus == OcaStatus.UNKNOWN) {
            try {
                hasSigned = OracleContributorAgreementService
                        .hasSignedOCAforOpenJDK(user.name);
            } catch (OCAComunicationException e) {
                LOGGER.error(e.getMessage(), e);
            }
        } else if (user.ocaStatus == OcaStatus.SIGNED) {
            hasSigned = true;
        } else if (user.ocaStatus == OcaStatus.NOT_SIGNED) {
            hasSigned = false;
        }
        return hasSigned;
    }
}
